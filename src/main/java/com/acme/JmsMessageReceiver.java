package com.acme;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
@RefreshScope
public class JmsMessageReceiver {
    private static final Logger logger = LoggerFactory.getLogger(JmsMessageReceiver.class);

    @Value("${sourceQueue}")
    String queueName;

    @JmsListener(destination = "${sourceQueue}")
    public void receive(String message) {
        logger.info("Received message [{}] from [{}]", message, queueName);
    }
}
